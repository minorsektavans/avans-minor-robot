*** Settings ***
Library                      SeleniumLibrary

*** Variable ***
${DOCKER}                    False
${TRAINING_NAME}             TestTraining
${ATTITUDE_INDEX}            0
${FUNCTIENIVEAU_INDEX}       1
${PASSINGSCRITERIA_VALUE}    1
${STATUS_INDEX}              1
${MAX_PARTICIPANTS}          5
${TRAINER_NAME}              TestTrainer
${TRAINER_EMAIL}             TestEmail@email.com
${TRAINING_DATE}             01-10-2020 18:00:00
${TRAINING_DESCRIPTION}      Dit is een testbeschrijving

*** Keywords ***
Setup Browser
    [Arguments]                         ${url}
    Run Keyword If                      '${DOCKER}' == 'True'                                                                                                                               Open Browser In Jenkins          ${url}
    Run Keyword If                      '${DOCKER}' != 'True'                                                                                                                               Open Browser On Local Machine    ${url}

Open Browser On Local Machine
    [Arguments]                         ${URL}
    Log                                 Running RobotFramework Local
    Set Selenium Speed                  ${SELENIUM_DELAY}
    Open Browser                        ${URL}                                                                                                                                              browser=chrome

Open Browser In Jenkins
    [Arguments]                         ${URL}
    Log                                 Running RobotFramework Jenkins
    Set Selenium Speed                  ${SELENIUM_DELAY}
    Import Library                      XvfbRobot
    Start Virtual Display               1920                                                                                                                                                1080
    Open Browser                        ${URL}                                                                                                                                              browser=firefox
    Set Window Size                     1920                                                                                                                                                1080

Select Cursus From Cursuslist
    [Arguments]                         ${TRAINING_NAME}
    Wait Until Page Contains Element    id=create                                                                                                                                           timeout=1                        error=Kan geen training selecteren, lijst is niet beschikbaar
    Click Element                       //*[contains(text(),'${TRAINING_NAME}')]

Click Cursus Details Button
    Wait Until Page Contains Element    //*[@class="expanded"]//*[@id="goToTrainingDetails"]                                                                                                timeout=1
    Click Element                       //*[@class="expanded"]//*[@id="goToTrainingDetails"]

Click Login Button
    Wait Until Page Contains Element    //*[@id="login"]                                                                                                                                    timeout=1
    Click Element                       //*[@id="login"]

Verify Login
    Wait Until Page Contains Element    //*[@id="create"]                                                                                                                                   timeout=1

Click Cursus Wijzigen Button
    Wait Until Page Contains Element    //*[@class="expanded"]//*[@id="editTraining"]                                                                                                       timeout=1
    Click Element                       //*[@class="expanded"]//*[@id="editTraining"]

Click Cursus Create Button
    Click Element                       //*[@id="create"]

Click Cursus Verwijderen Button
    Wait Until Page Contains Element    //*[@class="expanded"]//*[contains(text(),'Verwijderen')]                                                                                           timeout=1
    Click Element                       //*[@class="expanded"]//*[contains(text(),'Verwijderen')]

Wait until CreateForm Loaded
    Wait Until Page Contains Element    //*[@id="trainingName"]                                                                                                                             timeout=1

Input Cursus Form
    Wait until CreateForm Loaded
    Input Username
    Input Attitude
    Input Functieniveau
    Input Slagingscriterium
    Input Status
    Input Participants
    Input TeacherName
    Input TeacherEmail
    Input Data
    Input Description

Input Username
    Input Text                          //*[@id="trainingName"]                                                                                                                             ${TRAINING_NAME}

Input Attitude
    Select From List By Index           //*[@id="selectAttitude"]                                                                                                                           ${ATTITUDE_INDEX}

Input Functieniveau
    Select From List By Index           //*[@id="functieniveau"]                                                                                                                            ${FUNCTIENIVEAU_INDEX}

Input Slagingscriterium
    Select From List By Index           //*[@id="passingCriteria"]                                                                                                                          ${PASSINGSCRITERIA_VALUE}

Input Status
    Select From List By Index           //*[@id="status"]                                                                                                                                   ${STATUS_INDEX}

Input Participants
    Input Text                          //*[@id="maxParticipants"]                                                                                                                          ${MAX_PARTICIPANTS}

Input TeacherName
    Input Text                          //*[@id="trainerName"]                                                                                                                              ${TRAINER_NAME}

Input TeacherEmail
    Input Text                          //*[@id="trainerEmail"]                                                                                                                             ${TRAINER_EMAIL}

Input Data
    Input Text                          //*[@id="trainingDate"]                                                                                                                             ${TRAINING_DATE}

Input Description
    Input Text                          //*[@id="trainingDescription"]                                                                                                                      ${TRAINING_DESCRIPTION}

Click Save Button
    Click Element                       //*[@id="save"]

Click Aanmelden
    Wait Until Page Contains Element    id=aanmelden                                                                                                                                        timeout=1                        error=Kan geen training selecteren, lijst is niet beschikbaar
    Click Element                       id=aanmelden
    Wait Until Page Contains Element    id=ja                                                                                                                                               timeout=1                        error=Kan geen training selecteren, lijst is niet beschikbaar
    Click Element                       id=ja

Click Terug To Overzicht
    Click Element                       id=terug
    Wait Until Page Contains Element    id=create                                                                                                                                           timeout=1                        error=Kan geen training selecteren, lijst is niet beschikbaar

Check Created Cursus
    Element Text Should Be              //h2[contains(text(), "${TRAINING_NAME}")]                                                                                                          ${TRAINING_NAME}                 message=None                                                     ignore_case=False

Load Details Page
    Click Element                       //h2[contains(normalize-space(),'TestTraining')]
    Wait Until Element Is Visible       //div[@class='content transition active visible']//div//div[@id='goToTrainingDetails']
    Click Element                       //div[@class='content transition active visible']//div//div[@id='goToTrainingDetails']
    Wait Until Page Contains Element    id=aanmelden

Check Details Page
    Element Text Should Be              //*[@class="field"][contains(text(), "${TRAINING_NAME}")]                                                                                           ${TRAINING_NAME}                 message=None                                                     ignore_case=False
    Element Text Should Be              //*[@class="field"][contains(text(), "${TRAINING_DESCRIPTION}")]                                                                                    ${TRAINING_DESCRIPTION}          message=None                                                     ignore_case=False

Delete Cursus
    Select Cursus From Cursuslist       ${TRAINING_NAME}
    Click Cursus Verwijderen Button
    Wait Until Element Is Visible       id=yesImSure
    Click Element                       id=yesImSure
    Wait Until Element Is Not Visible   id=yesImSure

Click Logout
    Click Element                       id=logout
    Wait Until Element Is Visible       id=login
