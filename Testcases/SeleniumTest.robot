*** Settings ***
Resource             ../Keywords/SeleniumKeywords.robot
Suite Teardown       Close All Browsers
Documentation        Keyword documentation: https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html
...
...                  Kijk goed of er keywords zijn die je kan gebruiken in de Keywords/SeleniumKeywords.robot file

*** Variable ***
${SELENIUM_DELAY}    0
${TEST_URL}          http://cursusclient-${GROUP_NAME}-hco-kza-cursus.apps.prod.am5.hotcontainers.nl/cursussen
${GROUP_NAME}        hufflepuff

*** Test Cases ***
#Opdracht 0: Google Test
    # Als deze test werkt dan werkt RobotFramework goed. Verwijder deze als je begint.}
    #Setup Browser                       https://www.google.com
    #Set Selenium Speed                  ${SELENIUM_DELAY}
    #Wait Until Page Contains Element    name=q                    timeout=5

Opdracht 1: Login in de cursusclient
    Setup Browser                   ${TEST_URL}
    Click Login Button

Opdracht 2: Controleer of de pagina correct geladen is
    Verify Login

Opdracht 3: Open het scherm om een nieuwe training aan te maken
    Click Cursus Create Button

Opdracht 4: Maak een nieuwe training aan
    Input Cursus Form
    Click Save Button    

Opdracht 5: Meld je aan voor de training die je net hebt gemaakt
    Click Aanmelden
    Click Terug To Overzicht

Opdracht 6: Controleer of de aangemaakte training in de lijst van trainingen terugkomt
    Check Created Cursus

Opdracht 7: Controleer de details pagina van de aangemaakte training
    Load Details Page
    Check Details Page

Opdracht 8: Verwijder de aangemaakte training
    Click Terug To Overzicht
    Delete Cursus

Opdracht 9: Test de logout functionaliteit
    Click Logout