*** Settings ***
Resource                           ../Keywords/ApiTestKeywords.robot

*** Variables ***
${GROUP_NAME}                      hufflepuff
${URL}                             http://cursusservice-${GROUP_NAME}-hco-kza-cursus.apps.prod.am5.hotcontainers.nl/api
${NEW_CURSUS_DATUM}                01-01-2019 18:00:00
${NEW_CURSUS_DOCENT_NAAM}          Docent Naam
${NEW_CURSUS_DOCENT_EMAIL}         test@test.nl
${NEW_CURSUS_NAAM}                 Testtraining
${NEW_CURSUS_ATT}                  Technical attitude
${NEW_CURSUS_FN}                   3
${NEW_CURSUS_SLAGINGCR}            Zelfstudie
${NEW_CURSUS_STATUS}               Inschrijving geopend
${NEW_CURSUS_MAXDEELNMRS}          5
${NEW_CURSUSCURSIST_AANGEMAAKT}    Succesvol aangemeld voor de cursus.
${NEW_CURSUS_DESC}                 Beschrijving van de cursus
${NEW_TRAINING_JSON}               {"cursusdata":[{"id":null,"datum":"${NEW_CURSUS_DATUM}"}],
...                                "cursusdocenten":[{"id":null,"naam":"${NEW_CURSUS_DOCENT_NAAM}","email":"${NEW_CURSUS_DOCENT_EMAIL}"}],
...                                "cursuscursisten":[], "naam":"${NEW_CURSUS_NAAM}","attitude":"${NEW_CURSUS_ATT}",
...                                "functieniveau":"${NEW_CURSUS_FN}","slagingscriterium":"${NEW_CURSUS_SLAGINGCR}", "status":"${NEW_CURSUS_STATUS}",
...                                "maxdeelnemers":"${NEW_CURSUS_MAXDEELNMRS}","beschrijving":"${NEW_CURSUS_DESC}"}

${STUDENT_NAME1}                   Avans MinorstudentEen
${STUDENT_ID1}                     101
${STUDENT_EMAIL1}                  avansminorstudentEen@avans.nl
${STUDENT_NAME2}                   Andere MinorstudentTwee
${STUDENT_ID2}                     99
${STUDENT_EMAIL2}                  anderestudentTwee@avans.nl
${STUDENT_NAME3}                   Avans MinorstudentDrie
${STUDENT_ID3}                     100
${STUDENT_EMAIL3}                  anderestudentDrie@avans.nl
${STUDENT_NAME4}                   Avans MinorstudentFour
${STUDENT_ID4}                     102
${STUDENT_EMAIL4}                  anderestudentFour@avans.nl
${STUDENT_NAME5}                   Avans Minorstudent5
${STUDENT_ID5}                     103
${STUDENT_EMAIL5}                  anderestudent5@avans.nl
${STUDENT_NAME6}                   Avans Minorstudent6
${STUDENT_ID6}                     104
${STUDENT_EMAIL6}                  anderestudent6@avans.nl
${NEW_TRAINEE_JSON1}               {"email": "${STUDENT_EMAIL1}", "id": ${STUDENT_ID1}, "naam": "${STUDENT_NAME1}"}
${NEW_TRAINEE_JSON2}               {"email": "${STUDENT_EMAIL2}", "id": ${STUDENT_ID2}, "naam": "${STUDENT_NAME2}"}
${NEW_TRAINEE_JSON3}               {"email": "${STUDENT_EMAIL3}", "id": ${STUDENT_ID3}, "naam": "${STUDENT_NAME3}"}
${NEW_TRAINEE_JSON4}               {"email": "${STUDENT_EMAIL4}", "id": ${STUDENT_ID4}, "naam": "${STUDENT_NAME4}"}
${NEW_TRAINEE_JSON5}               {"email": "${STUDENT_EMAIL5}", "id": ${STUDENT_ID5}, "naam": "${STUDENT_NAME5}"}
${NEW_TRAINEE_JSON6}               {"email": "${STUDENT_EMAIL6}", "id": ${STUDENT_ID6}, "naam": "${STUDENT_NAME6}"}
*** Test Case ***

1. Controleer of de functieniveaus correct terugkomen
    GET Functieniveaus
    Response Status Should Be       200
    Log Response Body To Console

2. Creëer een nieuwe training in de applicatie
    POST New Training               ${NEW_TRAINING_JSON}
    Response Status Should Be       201
    Log Response Body To Console

3. Verifieer of de training uit stap 2 correct is opgeslagen
    GET Training                    ${CREATED_TRAINING_ID}
    Response Status Should Be       200
    #Verify Response JSON            ${NEW_TRAINING_JSON}
    Log Response Body To Console

4. Test of een nieuwe deelnemer kan worden toegevoegd
    POST New Participant            ${CREATED_TRAINING_ID}    ${NEW_TRAINEE_JSON1}
    Response Status Should Be       200
    Log Response Body To Console

5. Test of er niet meer dan het maximum aantal deelnemers kunnen aanmelden en dat je de verwachte foutmelding krijgt als dat wel zo is
    POST New Participant            ${CREATED_TRAINING_ID}    ${NEW_TRAINEE_JSON2}
    Response Status Should Be       200
    POST New Participant            ${CREATED_TRAINING_ID}    ${NEW_TRAINEE_JSON3}
    Response Status Should Be       200
    POST New Participant            ${CREATED_TRAINING_ID}    ${NEW_TRAINEE_JSON4}
    Response Status Should Be       200
    POST New Participant            ${CREATED_TRAINING_ID}    ${NEW_TRAINEE_JSON5}
    Response Status Should Be       200
    POST New Participant            ${CREATED_TRAINING_ID}    ${NEW_TRAINEE_JSON6}
    Response Status Should Be       400
    Log Response Body To Console

6. Delete de training die je hebt aangemaakt en valideer dat hij weg is
    DELETE Training                 ${CREATED_TRAINING_ID}
    GET Training                    ${CREATED_TRAINING_ID}
    Response Status Should Be       404




